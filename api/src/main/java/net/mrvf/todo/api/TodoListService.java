package net.mrvf.todo.api;

import net.mrvf.todo.model.CreateToDoRequest;
import net.mrvf.todo.model.ToDoTask;
import org.joda.time.DateTime;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface TodoListService {

    @GET
    List<ToDoTask.Summary> getOverview();

    @GET
    @Path("/{id}")
    ToDoTask getTodo(@PathParam("id") long id);

    @POST
    ToDoTask addTodo(CreateToDoRequest request);

    @DELETE
    @Path("/{id}")
    ToDoTask deleteTodo(@PathParam("id") long id);

    @PUT
    @Path("/{id}")
    ToDoTask toggleComplete(@PathParam("id") long id);
}
