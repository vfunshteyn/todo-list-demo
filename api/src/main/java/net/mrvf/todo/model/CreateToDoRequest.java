package net.mrvf.todo.model;

import org.joda.time.DateTime;

import java.util.Objects;

public class CreateToDoRequest {
    private final String summary;

    private final String details;

    private final DateTime dueDate;

    public CreateToDoRequest(String summary, String details, DateTime dueDate) {
        Objects.requireNonNull(summary);
        Objects.requireNonNull(details);
        Objects.requireNonNull(dueDate);

        this.summary = summary;
        this.details = details;
        this.dueDate = dueDate;
    }

    public String getSummary() {
        return summary;
    }

    public String getDetails() {
        return details;
    }

    public DateTime getDueDate() {
        return dueDate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CreateToDoRequest{");
        sb.append("summary='").append(summary).append('\'');
        sb.append(", details='").append(details).append('\'');
        sb.append(", dueDate=").append(dueDate);
        sb.append('}');
        return sb.toString();
    }
}
