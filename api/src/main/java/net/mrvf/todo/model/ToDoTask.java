package net.mrvf.todo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.joda.time.DateTime;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ToDoTask {

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public static class Summary {
        private final long id;

        private final String summary;

        private final boolean isCompleted;

        private final boolean isPastDue;

        public long getId() {
            return id;
        }

        public String getSummary() {
            return summary;
        }

        public boolean isCompleted() {
            return isCompleted;
        }

        public boolean isPastDue() {
            return isPastDue;
        }

        public Summary(long id, String summary, boolean isCompleted, boolean isPastDue) {
            this.id = id;
            this.summary = summary;
            this.isCompleted = isCompleted;
            this.isPastDue = isPastDue;
        }
    }

    private final long id;

    private final String summary;

    private final String details;

    private final DateTime dueDate;

    private boolean isCompleted;

    public ToDoTask(long id, String summary, String details, DateTime dueDate, boolean isCompleted) {
        this.id = id;
        this.summary = summary;
        this.details = details;
        this.dueDate = dueDate;
        this.isCompleted = isCompleted;
    }

    public long getId() {
        return id;
    }

    public String getSummary() {
        return summary;
    }

    public String getDetails() {
        return details;
    }

    public DateTime getDueDate() {
        return dueDate;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void toggleCompleted() {
        isCompleted = !isCompleted;
    }

    public Summary toSummary() {
        boolean isOverdue = !isCompleted && dueDate != null && dueDate.isBeforeNow();

        return new Summary(id, summary, isCompleted, isOverdue);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ToDoTask{");
        sb.append("id=").append(id);
        sb.append(", summary='").append(summary).append('\'');
        sb.append(", details='").append(details).append('\'');
        sb.append(", dueDate=").append(dueDate);
        sb.append(", isCompleted=").append(isCompleted);
        sb.append('}');
        return sb.toString();
    }
}
