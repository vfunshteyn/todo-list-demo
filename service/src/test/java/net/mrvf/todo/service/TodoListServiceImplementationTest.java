package net.mrvf.todo.service;

import net.mrvf.todo.model.CreateToDoRequest;
import net.mrvf.todo.model.ToDoTask;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class TodoListServiceImplementationTest {

    @Rule
    public final MockitoRule rule =
            MockitoJUnit.rule().strictness(Strictness.WARN);

    @Mock
    private ToDoTaskManager listManager;

    @InjectMocks
    private TodoListServiceImplementation serviceImplementation;

    @Test
    public void getOverview() {
        serviceImplementation.getOverview();
        verify(listManager).getTaskOverview();
    }

    @Test
    public void getTodo() {
        when(listManager.getTask(anyLong())).thenReturn(mock(ToDoTask.class));
        long id = 10;
        serviceImplementation.getTodo(id);
        verify(listManager).getTask(id);
    }

    @Test(expected = NotFoundException.class)
    public void getTodoNotFound() {
        when(listManager.getTask(anyLong())).thenReturn(null);
        serviceImplementation.getTodo(10);
    }

    @Test
    public void addTodo() {
        String summary = "todo", details = "details";
        DateTime due = DateTime.now();

        serviceImplementation.addTodo(new CreateToDoRequest(summary, details, due));
        verify(listManager).add(argThat(todo ->
            details.equals(todo.getDetails()) && summary.equals(todo.getSummary()) &&
            due.equals(todo.getDueDate()) && ! todo.isCompleted()
        ));
    }

    @Test(expected = InternalServerErrorException.class)
    public void addToDoDuplicate() {
        when(listManager.add(any(ToDoTask.class))).thenReturn(mock(ToDoTask.class));
        String summary = "todo", details = "details";
        DateTime due = DateTime.now();

        serviceImplementation.addTodo(new CreateToDoRequest(summary, details, due));
    }

    @Test
    public void deleteTodo() {
        long id = 10;
        when(listManager.remove(anyLong())).thenReturn(mock(ToDoTask.class));
        serviceImplementation.deleteTodo(id);
        verify(listManager).remove(id);
    }

    @Test(expected = NotFoundException.class)
    public void deleteTodoNotFound() {
        when(listManager.remove(anyLong())).thenReturn(null);
        serviceImplementation.deleteTodo(10);
    }

    @Test
    public void toggleComplete() {
        when(listManager.toggleCompleted(anyLong())).thenReturn(mock(ToDoTask.class));
        long id = 10;
        serviceImplementation.toggleComplete(id);
        verify(listManager).toggleCompleted(id);
    }

    @Test(expected = NotFoundException.class)
    public void toggleCompleteNotFound() {
        when(listManager.toggleCompleted(anyLong())).thenReturn(null);
        serviceImplementation.toggleComplete(10);
    }
}