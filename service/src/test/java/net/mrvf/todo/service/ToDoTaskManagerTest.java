package net.mrvf.todo.service;

import net.mrvf.todo.model.ToDoTask;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.*;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.notNullValue;


public class ToDoTaskManagerTest {

    private final ToDoTaskManager manager = new ToDoTaskManager();

    private ToDoTask task;

    @Before
    public void init() {
        task = mock(ToDoTask.class);
        when(task.getId()).thenReturn(1l);
    }

    @Test
    public void add() {
        assertThat(manager.add(task), nullValue());
    }

    @Test
    public void addFails() {
        assertThat(manager.add(task), nullValue());

        assertThat(manager.add(task), notNullValue());
    }

    @Test
    public void remove() {
        assertThat(manager.add(task), nullValue());
        assertThat(manager.remove(task.getId()), notNullValue());
    }

    @Test
    public void removeFails() {
        assertThat(manager.remove(task.getId()), nullValue());
    }

    @Test
    public void toggleCompleted() {
        ToDoTask task = new ToDoTask(555, "todo", "details",
                DateTime.now().plusDays(1), false);
        assertThat(manager.add(task), nullValue());

        assertThat(manager.toggleCompleted(task.getId()), sameInstance(task));

        assertThat(task.isCompleted(), is(true));
    }

    @Test
    public void toggleCompletedFails() {
        assertThat(manager.toggleCompleted(10), nullValue());
    }

    @Test
    public void getTaskOverview() {
        ToDoTask task = new ToDoTask(555, "todo", "details", DateTime.now().plusDays(1), false);
        assertThat(manager.add(task), nullValue());

        List<ToDoTask.Summary> overview = manager.getTaskOverview();
        assertThat(overview.size(), is(1));
        ToDoTask.Summary s = overview.get(0);

        assertThat(s.getId(), is(task.getId()));
        assertThat(s.getSummary(), is(task.getSummary()));
        assertThat(s.isCompleted(), is(false));
        assertThat(s.isPastDue(), is(false));
    }

    @Test
    public void getTaskOverviewPastDueTask() {
        ToDoTask task = new ToDoTask(555, "todo", "details", DateTime.now().minusDays(1), false);
        assertThat(manager.add(task), nullValue());

        List<ToDoTask.Summary> overview = manager.getTaskOverview();
        assertThat(overview.size(), is(1));
        ToDoTask.Summary s = overview.get(0);

        assertThat(s.getId(), is(task.getId()));
        assertThat(s.getSummary(), is(task.getSummary()));
        assertThat(s.isCompleted(), is(false));
        assertThat(s.isPastDue(), is(true));

        ToDoTask completed = manager.toggleCompleted(task.getId());
        assertThat(completed.isCompleted(), is(true));

        overview = manager.getTaskOverview();
        assertThat(overview.size(), is(1));
        s = overview.get(0);

        assertThat(s.getId(), is(task.getId()));
        assertThat(s.getSummary(), is(task.getSummary()));
        assertThat(s.isCompleted(), is(true));
        assertThat(s.isPastDue(), is(false));
    }

    @Test
    public void getTaskOverviewChangesVisible() {
        List<ToDoTask.Summary> overview = manager.getTaskOverview();
        assertThat(overview.size(), is(0));

        assertThat(manager.add(task), nullValue());

        overview = manager.getTaskOverview();
        assertThat(overview.size(), is(1));
    }

    @Test
    public void getTask() {
        assertThat(manager.getTask(task.getId()), nullValue());

        assertThat(manager.add(task), nullValue());

        assertThat(manager.getTask(task.getId()), sameInstance(task));

        assertThat(manager.remove(task.getId()), notNullValue());

        assertThat(manager.getTask(task.getId()), nullValue());
    }

}