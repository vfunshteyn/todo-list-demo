package net.mrvf.todo.config;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fasterxml.jackson.datatype.joda.cfg.JacksonJodaDateFormat;
import com.fasterxml.jackson.datatype.joda.deser.DateTimeDeserializer;
import com.fasterxml.jackson.datatype.joda.ser.DateTimeSerializer;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import net.mrvf.todo.api.TodoListService;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.feature.LoggingFeature;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.spring.JAXRSServerFactoryBeanDefinitionParser;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.ConfigurableEnvironment;

import java.util.Collections;
import java.util.List;

@Configuration
public class ServiceConfig {

    @Autowired
    protected ConfigurableEnvironment environment;

    @Bean(destroyMethod = "shutdown")
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

        JodaModule jodaModule = new JodaModule();
        JacksonJodaDateFormat dtFmt = new JacksonJodaDateFormat(DateTimeFormat.forPattern("yyyy-MM-dd HH:mm"));
        DateTimeSerializer dtSer = new DateTimeSerializer(dtFmt);
        JsonDeserializer dtDeser = new DateTimeDeserializer(DateTime.class, dtFmt);
        jodaModule.addSerializer(dtSer);
        jodaModule.addDeserializer(DateTime.class, dtDeser);

        mapper.registerModule(jodaModule);
        mapper.registerModule(new ParameterNamesModule());
        return mapper;
    }

    /**
     * <p>jaxrsServerFactoryBean.</p>
     *
     * @return a {@link org.apache.cxf.jaxrs.JAXRSServerFactoryBean} object.
     */
    @Bean
    @DependsOn("cxf")
    public JAXRSServerFactoryBean jaxrsServerFactoryBean(ObjectMapper objectMapper) {
        JAXRSServerFactoryBean serverFactoryBean = new JAXRSServerFactoryBeanDefinitionParser.SpringJAXRSServerFactoryBean();
        serverFactoryBean.setAddress("/");
        // debug logging
        if (environment.getProperty("debug") != null) {
            serverFactoryBean.getFeatures().add(new LoggingFeature());
        }
        JacksonJsonProvider provider = new JacksonJsonProvider(objectMapper);

        serverFactoryBean.setProviders(Collections.singletonList(provider));
        return serverFactoryBean;
    }

    @Bean
    public Server server(JAXRSServerFactoryBean jaxrsServerFactoryBean, TodoListService mainService)
    {
        List<Object> services = Collections.singletonList(mainService);
        jaxrsServerFactoryBean.setServiceBeans(services);

        return jaxrsServerFactoryBean.create();
    }

}
