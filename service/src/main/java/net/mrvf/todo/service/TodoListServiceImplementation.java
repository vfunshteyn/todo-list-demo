package net.mrvf.todo.service;

import net.mrvf.todo.api.TodoListService;
import net.mrvf.todo.model.CreateToDoRequest;
import net.mrvf.todo.model.ToDoTask;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class TodoListServiceImplementation implements TodoListService {

    private final AtomicLong todoId = new AtomicLong(1);

    @Autowired
    private ToDoTaskManager listManager;

    @Override
    public List<ToDoTask.Summary> getOverview() {
        return listManager.getTaskOverview();
    }

    @Override
    public ToDoTask getTodo(long id) {
        ToDoTask todo = listManager.getTask(id);
        if (todo == null) {
            String msg = String.format("Todo task with id %d not found", id);
            throw new NotFoundException(msg, getResponse(msg, Response.Status.NOT_FOUND));
        }
        return todo;
    }

    @Override
    public ToDoTask addTodo(CreateToDoRequest request) {
        ToDoTask entry = new ToDoTask(todoId.getAndIncrement(), request.getSummary(),
                request.getDetails(), request.getDueDate(), false);
        ToDoTask existing = listManager.add(entry);

        if (existing != null) {
            String msg = String.format("Entry %s already exists", entry);
            throw new InternalServerErrorException(getResponse(msg, Response.Status.INTERNAL_SERVER_ERROR));
        }
        return entry;
    }

    @Override
    public ToDoTask deleteTodo(long id) {
        ToDoTask removed = listManager.remove(id);
        if (removed == null) {
            String msg = String.format("Todo task with id %d not found", id);
            throw new NotFoundException(msg, getResponse(msg, Response.Status.NOT_FOUND));
        }
        return removed;
    }

    @Override
    public ToDoTask toggleComplete(long id) {
        ToDoTask updated = listManager.toggleCompleted(id);
        if (updated == null) {
            String msg = String.format("Todo task with id %d not found", id);
            throw new NotFoundException(msg, getResponse(msg, Response.Status.NOT_FOUND));
        }
        return updated;
    }

    private static Response getResponse(String message, Response.Status status) {
        Map<String, String> content = Collections.singletonMap("error", message);
        return Response.status(status).entity(content).build();
    }
}
