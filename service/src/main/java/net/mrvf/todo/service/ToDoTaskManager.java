package net.mrvf.todo.service;

import net.mrvf.todo.model.ToDoTask;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

@Component
public class ToDoTaskManager {

    private final ConcurrentMap<Long, ToDoTask> todoList = new ConcurrentHashMap<>();

    /**
     * Add new task
     * @param entry new task to add
     * @return existing task or <code>null</code> if operation succeeded
     */
    public ToDoTask add(ToDoTask entry) {
        return todoList.putIfAbsent(entry.getId(), entry);
    }

    /**
     * Remove task
     * @param id task to be removed
     * @return removed task or <code>null</code> if not found
     */
    public ToDoTask remove(long id) {
        return todoList.remove(id);
    }

    /**
     * Toggle completed flag on or off
     * @param id task to be modified
     * @return modified task or <code>null</code> if not found
     */
    public ToDoTask toggleCompleted(long id) {
        return todoList.computeIfPresent(id, (taskId, toDoTask) -> {
            toDoTask.toggleCompleted();
            return toDoTask;
        });
    }

    /**
     * Return overview of managed tasks
     * @return List of tasks in "light" form
     */
    public List<ToDoTask.Summary> getTaskOverview() {
        return todoList.values().stream().map(ToDoTask::toSummary).collect(Collectors.toList());
    }

    /**
     * Look up task by its id
     * @param id
     * @return retrieved task or <code>null</code> if not found
     */
    public ToDoTask getTask(long id) {
        return todoList.get(id);
    }
}
