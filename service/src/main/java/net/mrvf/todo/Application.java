package net.mrvf.todo;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.*;

@SpringBootApplication
public class Application
{
    public static void main(String[] args)
    {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public ServletRegistrationBean cxfServlet()
    {
        CXFServlet servlet = new CXFServlet();

        ServletRegistrationBean registrationBean = new ServletRegistrationBean(servlet, "/todo-list/*");
        registrationBean.setLoadOnStartup(1);

        return registrationBean;
    }

}
