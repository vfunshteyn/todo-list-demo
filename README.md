# README #

This repository contains an example web service application - TODO list. It is packaged as self-contained Maven project 
that builds into a Spring Boot application.

#### Pre-requisites
* Java 8
* Maven 3.x

#### Build it

1. `git clone git@bitbucket.org:vfunshteyn/todo-list-demo.git`
2. `cd todo-list-demo`
3. `mvn install`

#### Run it
1. `cd service`
2. `mvn spring-boot:run`

This will start the service inside an embedded Tomcat container on port 8080.

#### Test it

Unit tests are run as part of the build, for integration testing I recommend using a UI-based REST client like Postman.
Examples below use cURL.

##### Add a new ToDo item

* Request:

`curl -i -X POST http://localhost:8080/todo-list -H 'accept: application/json' -H 'content-type: application/json' \
  -d '{ "summary" : "Make car service appointment", "details" : "60K service", "dueDate": "2017-09-10 10:00"}'`

* Response:

``` json
{
  "id" : 1,
  "summary" : "Make car service appointment",
  "details" : "60K service",
  "dueDate" : "2017-09-10 10:00",
  "completed" : false
}
```

##### Get Overview of known ToDo items

* Request:

`curl -i -X GET http://localhost:8080/todo-list/ -H 'accept: application/json'`

* Response:

``` json
[
    {
        "id": 1,
        "summary": "Make car service appointment",
        "completed": false,
        "pastDue": false
    }
]
```

Note: if the due date of the originally added task is before the time of the request, the `pastDue` field would be set to `true`

##### Get details of specific ToDo item

* Request:

`curl -i -X GET http://localhost:8080/todo-list/1 -H 'accept: application/json'`

* Response:

``` json
{
    "id": 1,
    "summary": "Make car service appointment",
    "details": "60K service",
    "dueDate": "2017-09-10 10:00",
    "completed": false
}
```

##### Mark/Unmark ToDo item completed

* Request:
 
`curl -i -X PUT http://localhost:8080/todo-list/1 -H 'accept: application/json' -H 'content-type: application/json'`

* Response:

``` json
{
    "id": 1,
    "summary": "Make car service appointment",
    "details": "60K service",
    "dueDate": "2017-09-10 10:00",
    "completed": true
}
```
 
##### Delete existing ToDo item

* Request:

`curl -i -X DELETE http://localhost:8080/todo-list/1 -H 'accept: application/json' -H 'content-type: application/json'`

* Response:

``` json 
{
   "id" : 1,
   "summary" : "Make car service appointment",
   "details" : "60K service",
   "dueDate" : "2017-09-10 10:00",
   "completed" : false
 }
```
 
##### Error Handling
 
 1. All fields in initial create todo request are required, if one is missing an error is returned (currently a 500)
 2. Any operations that reference to non-existent todo items are handled by returning an error with standard 404 status, 
 e.g.
 
``` json 
{
   "error": "Todo task with id 2 not found"
}
```